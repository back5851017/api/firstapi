import { Router } from "express";
import { usersTemplate } from "../utils/user.js";
import { USERS as UserMock } from "../../../../../mocks/users.js";

const router = Router();

router.get("/:id", function (req, res, next) {
  const { id } = req.params;
  const user = UserMock.find((user) => user.id === id);
  if (!user) return next();
  res.render("template", {
    ...usersTemplate,
    userId: user.id,
  });
});

export default router;
