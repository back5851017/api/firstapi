import { Router } from "express";
import { usersTemplate } from "../utils/user.js";

const router = Router();

router.get("/", function (req, res, next) {
  res.render("template", { ...usersTemplate });
});

export default router;
