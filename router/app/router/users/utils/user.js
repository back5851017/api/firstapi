import { USERS as UserMock } from "../../../../../mocks/users.js";

export const usersTemplate = {
  title: "Utilisateurs",
  contentFile: "users",
  style: "users",
  users: UserMock,
  userId: null,
};
