import { Router } from "express";
import indexRoutes from "./routes/index.js";
import userWithIdRoutes from "./routes/userWithId.js";

const router = Router();

router.use(indexRoutes);
router.use(userWithIdRoutes);

export default router;
