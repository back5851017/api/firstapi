import { Router } from "express";
import { USERS as UsersMock } from "../../../../../mocks/users.js";

const router = Router();

const loginTemplate = {
  title: "Se connecter",
  contentFile: "login",
  style: "login",
  error: null,
};

router.get("/login", function (req, res, next) {
  res.render("template", { ...loginTemplate });
});

router.post("/login", function (req, res, next) {
  const { name, password } = req.body;
  const user = UsersMock.find(
    (user) => user.login === name && user.password === password
  );
  if (user) {
    req.session.user = { ...user };
    res.redirect("/app");
  } else {
    res.render("template", {
      ...loginTemplate,
      error: "Email ou mot de passe incorrect",
    });
  }
});

export default router;
