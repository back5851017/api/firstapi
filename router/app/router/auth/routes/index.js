import { Router } from "express";

const router = Router();

router.get("/", function (req, res, next) {
  if (req.session.user) {
    res.redirect("/auth/logout");
  } else {
    res.redirect("/auth/login");
  }
});

export default router;
