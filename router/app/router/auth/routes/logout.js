import { Router } from "express";

const router = Router();

router.get("/logout", function (req, res, next) {
  req.session.destroy();
  res.redirect("/app");
});

export default router;
