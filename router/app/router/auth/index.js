import { Router } from "express";

import indexRoutes from "./routes/index.js";
import loginRoutes from "./routes/login.js";
import logoutRoutes from "./routes/logout.js";

const router = Router();

router.use(indexRoutes);
router.use(loginRoutes);
router.use(logoutRoutes);

export default router;
