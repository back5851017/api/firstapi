import { Router } from "express";
import { writeFile } from "fs";
import { fileWithDate } from "../utils/date.js";

const router = Router();

router.get("/withfs", function (req, res, next) {
  // With fs module write file on server and download to client
  const { fileContent, fileName } = fileWithDate();

  writeFile(fileName, fileContent, function (err) {
    if (err) {
      res.status(500).send("Something went wrong to download file");
      console.error(err);
    } else res.download(fileName);
  });
});

export default router;
