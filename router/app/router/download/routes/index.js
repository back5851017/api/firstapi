import { Router } from "express";
import { fileWithDate } from "../utils/date.js";

const router = Router();

router.get("/", function (req, res, next) {
  const { fileContent, fileName } = fileWithDate();
  res.setHeader("Content-disposition", "attachment; filename=" + fileName);
  res.setHeader("Content-type", "text/plain");

  res.send(fileContent);
});

export default router;
