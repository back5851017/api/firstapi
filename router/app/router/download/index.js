import express from "express";

import indexRoutes from "./routes/index.js";
import withFsRoutes from "./routes/withfs.js";

const router = express.Router();

router.use(indexRoutes);
router.use(withFsRoutes);

export default router;
