export const fileWithDate = () => {
  const currentDate = new Date();
  const fileName = `${currentDate.getFullYear()}${(currentDate.getMonth() + 1)
    .toString()
    .padStart(2, "0")}${currentDate
    .getDate()
    .toString()
    .padStart(2, "0")}_${currentDate
    .getHours()
    .toString()
    .padStart(2, "0")}${currentDate
    .getMinutes()
    .toString()
    .padStart(2, "0")}${currentDate
    .getSeconds()
    .toString()
    .padStart(2, "0")}.txt`;
  const fileContent = `Today's date is: ${currentDate.toDateString()}`;
  return { fileName, fileContent };
};
