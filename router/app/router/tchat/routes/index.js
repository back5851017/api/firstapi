import { Router } from "express";

const router = Router();

router.get("/", function (req, res, next) {
  if (!req.session.user) return res.redirect("/auth/login");
  res.render("template", {
    title: "Tchat",
    contentFile: "tchat",
    style: "tchat",
  });
});

export default router;
