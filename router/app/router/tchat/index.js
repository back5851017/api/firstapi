import { Router } from "express";
import indexRoutes from "./routes/index.js";

const router = Router();

router.use(indexRoutes);

export default router;
