import { Router } from "express";

import homeRouter from "./router/home/index.js";
import usersRouter from "./router/users/index.js";
import authRouter from "./router/auth/index.js";
import downloadRouter from "./router/download/index.js";
import tchatRouter from "./router/tchat/index.js";

const router = Router();

router.use("/", homeRouter);
router.use("/users", usersRouter);
router.use("/auth", authRouter);
router.use("/download", downloadRouter);
router.use("/tchat", tchatRouter);

export default router;
