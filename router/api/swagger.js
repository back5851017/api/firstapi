import swaggerJsDoc from "swagger-jsdoc";
import swaggerUi from "swagger-ui-express";

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: "API Documentation 1.0",
      description: "API Information",
      contact: {
        name: "Tisma",
      },
      servers: ["http://localhost:8080/api/rest"],
    },
  },
  apis: ["./router/**/*.js"],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);

export { swaggerUi, swaggerDocs };
