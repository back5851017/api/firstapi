export default function (sequelize, DataTypes) {
  return sequelize.define(
    "Concert",
    {
      idConcert: {
        autoIncrement: true,
        type: DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
        primaryKey: true,
      },
      dateConcert: {
        type: DataTypes.DATEONLY,
        allowNull: true,
      },
      nbrPlaceDisponible: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      idVille: {
        type: DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
        references: {
          model: "Ville",
          key: "idVille",
        },
      },
    },
    {
      sequelize,
      tableName: "Concert",
      timestamps: false,
      indexes: [
        {
          name: "PRIMARY",
          unique: true,
          using: "BTREE",
          fields: [{ name: "idConcert" }],
        },
        {
          name: "idVille",
          using: "BTREE",
          fields: [{ name: "idVille" }],
        },
      ],
    }
  );
}
