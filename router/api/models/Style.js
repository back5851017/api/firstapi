export default function (sequelize, DataTypes) {
  return sequelize.define(
    "Style",
    {
      idStyle: {
        autoIncrement: true,
        type: DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
        primaryKey: true,
      },
      libelle: {
        type: DataTypes.STRING(50),
        allowNull: true,
      },
      description: {
        type: DataTypes.STRING(5000),
        allowNull: true,
      },
    },
    {
      sequelize,
      tableName: "Style",
      timestamps: false,
      indexes: [
        {
          name: "PRIMARY",
          unique: true,
          using: "BTREE",
          fields: [{ name: "idStyle" }],
        },
      ],
    }
  );
}
