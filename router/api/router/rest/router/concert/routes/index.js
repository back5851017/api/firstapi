import { Router } from "express";
import initModels from "../../../../../models/init-models.js";

const router = Router();

const { Concert } = initModels();

/**
 * @swagger
 * /concerts:
 *   get:
 *     summary: Récupérer la liste de tous les concerts
 *     description: Récupère la liste complète de tous les concerts.
 *     tags: [Concerts]
 *     responses:
 *       200:
 *         description: Succès. Retourne la liste complète des concerts.
 *         content:
 *         schema:
 *          type: array
 *          $ref: '#/definitions/Concert'
 */
router.get("/", async (req, res) => {
  const findConcert = await Concert.findAll();
  res.json(findConcert);
});

export default router;
