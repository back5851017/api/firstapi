import { Router } from "express";
import IndexConcertRoutes from "./routes/index.js";

const router = Router();

/**
 * @swagger
 * tags:
 *  name: Concerts
 * description: Concert management
 *
 * definitions:
 *   Concert:
 *     required:
 *       - IdArtiste
 *       - idStyle
 *     properties:
 *       idConcert:
 *         type: integer
 *       dateConcert:
 *         type: string
 *       nbrPlaceDisponible:
 *         type: integer
 *       idVille:
 *        type: integer
 */

router.use(IndexConcertRoutes);

export default router;
