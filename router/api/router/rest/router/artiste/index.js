import { Router } from "express";
import IndexArtisteRoutes from "./routes/index.js";
import ArtisteConcertsRoutes from "./routes/concert.js";

const router = Router();

/**
 * @swagger
 * tags:
 *  name: Artistes
 *  description: Artiste management
 *
 * definitions:
 *   Artiste:
 *     required:
 *       - IdArtiste
 *       - idStyle
 *     properties:
 *       IdArtiste:
 *         type: integer
 *       pseudo:
 *         type: string
 *       idStyle:
 *         type: integer
 */

router.use(IndexArtisteRoutes);
router.use(ArtisteConcertsRoutes);

export default router;
