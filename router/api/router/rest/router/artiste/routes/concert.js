import { Router } from "express";
import initModels from "../../../../../models/init-models.js";

const router = Router();

const { Realise, Concert } = initModels();

/**
 * @swagger
 * /concerts/{id}:
 *   get:
 *     summary: Récupérer la liste des concerts d'un artiste
 *     description: Récupère la liste des concerts associés à un artiste spécifique.
 *     tags: [Artistes, Concerts]
 *     parameters:
 *       - in: path
 *         name: id
 *         description: ID de l'artiste
 *         required: true
 *         schema:
 *           type: integer
 *     responses:
 *       200:
 *        description: Succès
 *        schema:
 *          type: array
 *          $ref: '#/definitions/Concert'
 */
router.get("/:id/concerts", async (req, res) => {
  //TODO optimiser requete pour ne pas faire de boucle
  const findRealise = await Realise.findAll({
    where: {
      idArtiste: req.params.id,
    },
  });

  const concerts = [];

  for (const realise of findRealise) {
    const concert = await Concert.findByPk(realise.idConcert);
    concerts.push(concert);
  }

  res.json(concerts);
});

export default router;
