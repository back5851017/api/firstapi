import { Router } from "express";
import initModels from "../../../../../models/init-models.js";

const router = Router();

const { Artiste } = initModels();

/**
 * @swagger
 * /artistes:
 *   get:
 *     summary: Récupérer la liste des artistes
 *     description: Récupère la liste complète des artistes.
 *     tags: [Artistes]
 *     responses:
 *       200:
 *        description: Succès
 *        schema:
 *          type: array
 *          $ref: '#/definitions/Artiste'
 */
router.get("/", async (req, res) => {
  const findArtistes = await Artiste.findAll();
  res.json(findArtistes);
});

export default router;
