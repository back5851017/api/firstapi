import { Router } from "express";
import IndexStyleRoutes from "./routes/index.js";

const router = Router();

/**
 * @swagger
 * tags:
 *  name: Styles
 * description: Style management
 *
 * definitions:
 *   Style:
 *     required:
 *       - idStyle
 *     properties:
 *       idStyle:
 *         type: integer
 *       libelle:
 *         type: string
 *       description:
 *         type: string
 *
 */
router.use(IndexStyleRoutes);

export default router;
