import { Router } from "express";
import initModels from "../../../../../models/init-models.js";

const router = Router();

const { Style } = initModels();

/**
 * @swagger
 * /styles:
 *   get:
 *     summary: Récupérer la liste de tous les styles musicaux
 *     description: Récupère la liste complète de tous les styles musicaux.
 *     tags: [Styles]
 *     responses:
 *       200:
 *        description: Succès
 *        schema:
 *          type: array
 *          $ref: '#/definitions/Style'
 */
router.get("/", async (req, res) => {
  const findStyles = await Style.findAll();
  res.json(findStyles);
});

export default router;
