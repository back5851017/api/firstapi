import { Router } from "express";
import initModels from "../../../../../models/init-models.js";

const router = Router();

const { Concert, Joue } = initModels();

/**
 * @swagger
 * /villes/{id}/style/{idStyle}/concerts:
 *   get:
 *     summary: Récupérer la liste des concerts d'un style pour un artiste
 *     description: Récupère la liste des concerts associés à un style spécifique pour un artiste spécifique.
 *     tags: [Villes,  Concerts]
 *     parameters:
 *       - in: path
 *         name: id
 *         description: ID de l'artiste
 *         required: true
 *         schema:
 *           type: integer
 *       - in: path
 *         name: idStyle
 *         description: ID du style musical
 *         required: true
 *         schema:
 *           type: integer
 *     responses:
 *       200:
 *        description: Succès
 *        schema:
 *          type: array
 *          $ref: '#/definitions/Concert'
 */
router.get("/:id/style/:idStyle/concerts", async (req, res) => {
  //TODO optimiser requete pour ne pas faire de boucle
  const findJoues = await Joue.findAll({
    where: {
      idStyle: req.params.idStyle,
    },
  });

  const concerts = [];

  for (const joue of findJoues) {
    const concert = await Concert.findByPk(joue.idConcert);
    concerts.push(concert);
  }

  res.json(concerts);
});

export default router;
