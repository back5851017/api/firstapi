import { Router } from "express";
import initModels from "../../../../../models/init-models.js";

const router = Router();

const { Visiteur } = initModels();

/**
 * @swagger
 * /villes/{id}/visiteurs:
 *   get:
 *     summary: Récupérer la liste des visiteurs d'une ville
 *     description: Récupère la liste des visiteurs associés à une ville spécifique.
 *     tags: [Villes, Visiteurs]
 *     parameters:
 *       - in: path
 *         name: id
 *         description: ID de la ville
 *         required: true
 *         schema:
 *           type: integer
 *     responses:
 *       200:
 *        description: Succès
 *        schema:
 *          type: array
 *          $ref: '#/definitions/Visitor'
 */
router.get("/:id/visiteurs", async (req, res) => {
  const findVisitors = await Visiteur.findAll({
    where: {
      idVille: req.params.id,
    },
  });
  res.json(findVisitors);
});

export default router;
