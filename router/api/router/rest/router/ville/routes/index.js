import { Router } from "express";
import initModels from "../../../../../models/init-models.js";

const router = Router();

const { Ville } = initModels();

/**
 * @swagger
 * /villes:
 *   get:
 *     summary: Récupérer la liste de toutes les villes
 *     description: Récupère la liste complète de toutes les villes.
 *     tags: [Villes]
 *     responses:
 *       200:
 *        description: Succès
 *        schema:
 *          type: array
 *          $ref: '#/definitions/Ville'
 */
router.get("/", async (req, res) => {
  const findVille = await Ville.findAll();
  res.json(findVille);
});

export default router;
