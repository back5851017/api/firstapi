import { Router } from "express";
import initModels from "../../../../../models/init-models.js";

const router = Router();

const { Concert } = initModels();

/**
 * @swagger
 * /villes/{id}/concerts:
 *   get:
 *     summary: Récupérer la liste des concerts dans une ville
 *     description: Récupère la liste des concerts associés à une ville spécifique.
 *     tags: [Villes, Concerts]
 *     parameters:
 *       - in: path
 *         name: id
 *         description: ID de la ville
 *         required: true
 *         schema:
 *           type: integer
 *     responses:
 *       200:
 *        description: Succès
 *        schema:
 *          type: array
 *          $ref: '#/definitions/Concert'
 */
router.get("/:id/concerts", async (req, res) => {
  const findConcerts = await Concert.findAll({
    where: {
      idVille: req.params.id,
    },
  });
  res.json(findConcerts);
});

export default router;
