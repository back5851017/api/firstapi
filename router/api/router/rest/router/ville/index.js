import { Router } from "express";
import IndexVillesRoutes from "./routes/index.js";
import VilleConcertsRoutes from "./routes/concerts.js";
import VilleVisiteursRoutes from "./routes/visitors.js";
import VilleStyleConcertsRoutes from "./routes/styleConcerts.js";

const router = Router();

/**
 * @swagger
 * tags:
 *  name: Villes
 * description: Concert management
 * definitions:
 *   Ville:
 *     required:
 *       - idVille
 *     properties:
 *       idVille:
 *         type: integer
 *       nom:
 *         type: string
 *       coordonnees:
 *         type: intege
 *
 *   Visitor:
 *     required:
 *       - idVisiteur
 *     properties:
 *       idVisiteur:
 *         type: integer
 *       nom:
 *         type: string
 *       prenom:
 *         type: string
 *       email:
 *         type: string
 *       age:
 *         type: integer
 *       adresse:
 *         type: string
 *       idParrain:
 *         type: integer
 *       idVille:
 *         type: integer
 *
 */

router.use(IndexVillesRoutes);
router.use(VilleConcertsRoutes);
router.use(VilleVisiteursRoutes);
router.use(VilleStyleConcertsRoutes);

export default router;
