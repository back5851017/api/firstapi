import { Router } from "express";

import ArtisteRouter from "./router/artiste/index.js";
import StylesRouter from "./router/style/index.js";
import ConcertRouter from "./router/concert/index.js";
import VilleRouter from "./router/ville/index.js";

const router = Router();

router.use("/artistes", ArtisteRouter);
router.use("/styles", StylesRouter);
router.use("/concerts", ConcertRouter);
router.use("/villes", VilleRouter);

export default router;
