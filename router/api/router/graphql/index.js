import { Router } from "express";
import { graphqlHTTP } from "express-graphql";
import { makeExecutableSchema } from "@graphql-tools/schema";
import initModels from "../../models/init-models.js";

const router = Router();

const { Concert } = initModels();

const typeDefs = `
scalar Date
type Query {
  concert(idVille: Int): [Concert]
}
type Concert {
  idConcert: Int
  dateConcert: Date
  nbrPlaceDisponible: Int
  idVille: Int
}
`;

const resolvers = {
  Query: {
    concert: async (_, { idVille }) => {
      try {
        const concerts = await Concert.findAll({ where: { idVille: idVille } });

        if (!concerts.length) {
          throw new Error(`No concert found with idVille: ${idVille}`);
        }
        return concerts;
      } catch (error) {
        console.error(error);
        throw new Error("Error retrieving concert data");
      }
    },
  },
};

const schema = makeExecutableSchema({ typeDefs, resolvers });

router.use(
  graphqlHTTP({
    schema: schema,
    graphiql: true,
  })
);

export default router;
