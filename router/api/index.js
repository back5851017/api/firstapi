import { Router } from "express";
import RestRouter from "./router/rest/index.js";
import GraphqlRouter from "./router/graphql/index.js";
import { swaggerUi, swaggerDocs } from "./swagger.js";

const router = Router();

router.use("/rest", RestRouter);
router.use("/graphql", GraphqlRouter);
router.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

export default router;
