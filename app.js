// ENV
import dotenv from "dotenv";
dotenv.config();

// EXPRESS
import express from "express";
import { createServer } from "http";

const app = express();
const httpServer = createServer(app);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(express.static("public"));

// VIEW ENGINE SETUP
app.set("views", "views/pages");
app.set("view engine", "ejs");

// COOKIES
import cookieParser from "cookie-parser";

app.use(cookieParser());

// MORGAN LOGGER
import logger from "morgan";

app.use(logger("dev"));

// BODY PARSER
import bodyParser from "body-parser";

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// SESSION
import session from "express-session";

const sessionMiddleware = session({
  secret: process.env.SESSION_SECRET_KEY,
  cookie: { maxAge: parseInt(process.env.COOKIE_DURATION) },
  resave: false,
  saveUninitialized: true,
});

app.use(sessionMiddleware);
app.use(function (req, res, next) {
  res.locals.user = req.session?.user;
  next();
});

// ROUTES
import appRouter from "./router/app/index.js";
import apiRouter from "./router/api/index.js";

app.use("/app", appRouter);
app.use("/api", apiRouter);

// SOCKET IO
import socketio from "./services/socketio/index.js";
socketio(httpServer, sessionMiddleware);

// ERROR HANDLER
import createError from "http-errors";

app.use(function (req, res, next) {
  next(createError(404));
});

app.use(function (err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};
  if (err.status === 404)
    return res.render("template", {
      title: "404",
      contentFile: "404",
      style: "404",
    });
  res.status(err.status || 500);
  res.render("error");
});

// START SERVER
httpServer.listen(process.env.WEB_PORT, function () {
  console.log(`listening on localhost:${process.env.WEB_PORT}`);
});
