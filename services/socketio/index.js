import { Server } from "socket.io";
import { SERVER_USER } from "../../mocks/users.js";
import { BAD_WORDS } from "../../constants.js";

const formatMessage = (user, text, isServerMessage) => {
  return {
    user: isServerMessage ? SERVER_USER : user,
    text,
    time: new Date().toLocaleTimeString(),
    id: Math.random()
      .toString(36)
      .substring(2, 20 + 2),
  };
};

const socketio = (httpServer, sessionMiddleware) => {
  const io = new Server(httpServer, {});

  io.engine.use(sessionMiddleware);

  const messages = [];
  const typingUsers = new Set();

  io.on("connection", function (socket) {
    const { user } = socket.request.session;

    // Filter bad words
    const badWordsRegex = new RegExp(`\\b(${BAD_WORDS.join("|")})\\b`, "gi");

    const sendMessage = (message, isServerMessage) => {
      const filteredMessage = message.replace(badWordsRegex, "*****");
      const msg = formatMessage(user, filteredMessage, isServerMessage);
      messages.push(msg);
      io.emit("chat_message", msg);
    };

    socket.emit("chat_message", messages);

    console.log(messages);

    socket.on(
      "user_join",
      () => user && sendMessage(`${user.name} vient de se connecter`, true)
    );

    socket.on("chat_message", ({ message }) => sendMessage(message, false));

    socket.on(
      "user_leave",
      () => user && sendMessage(`${user?.name} vient de se déconnecter`, true)
    );

    socket.on("delete_message", (messageId, userId) => {
      if (user.id !== userId) return;
      const index = messages.indexOf(
        messages.find((message) => message.id === messageId)
      );
      if (index !== -1) {
        messages.splice(index, 1);
        io.emit("chat_message", messages);
      }
    });

    socket.on("typing", (isTyping) => {
      if (!user) return;
      isTyping ? typingUsers.add(user) : typingUsers.delete(user);
      socket.broadcast.emit("typing", { users: Array.from(typingUsers) });
    });
  });
};

export default socketio;
