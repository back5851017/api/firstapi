# README

# Table of contents

- [Node Version: 20.9.0](#node-version-2090)
- [Server Setup](#server-setup)
  - [Install Dependencies](#install-dependencies)
  - [Run in Debug Mode](#run-in-debug-mode)
  - [Start the Server](#start-the-server)
- [Database Information](#database-information)
  - [phpMyAdmin](#phpmyadmin)
- [Server Details](#server-details)
- [Admin User Credentials](#admin-user-credentials)
- [Other User Information](#other-user-information)
- [Application Links](#application-links)
  - [Authentication](#authentication)
  - [User Management](#user-management)
  - [File Operations](#file-operations)
  - [Chat Feature](#chat-feature)
- [API Doc](#api)
- [GraphQL](#graphql)

## Node Version: 20.9.0

### Server Setup

1. **Install Dependencies:**

   ```bash
   npm install
   ```

2. **Run in Debug Mode:**

   ```bash
   npm run dev
   ```

3. **Start the Server:**

   ```bash
   npm start
   ```

   Both `npm run dev` and `npm start` launch the server with Docker and Express.

### Database Information

- **Database:** MariaDB
- **Port:** 4000

### phpMyAdmin

- **Port:** 4001
- **Login:** root
- **Password:** rootpwd

### Server Details

- **Port:** 8080

### Admin User Credentials

- **Login:** `admin`
- **Password:** `admin`

### Other User Information

Check the `mocks` folder for details on other users.

### Application Links

- `/app` for the home page

(Prefix for the following routes)

#### Authentication:

- `/auth/login` for login
- `/auth/logout` for logout

#### User Management:

- `/users` for all users
- `/user/:id` for a specific user
- _(Potential enhancement: Add a search bar for user lookup)_

#### File Operations:

- `/files` for downloading files

#### Chat Feature:

- `/tchat` to join the chat (requires login)
  - Delete your own messages
  - See who is currently typing

### API:

- Refere to swagger documentation on `/api/docs`

### GraphQL:

- `/api/graphql`
